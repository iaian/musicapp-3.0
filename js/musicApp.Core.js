/* Music App Core JS
 *  -  This file should be core JS functionallity to the musicApp
 *
 */

/* Gloabal Vars
 *  STEREO - I decided it would be AWESOME to name this main variable STEREO,
 *						it is and audio element.
 */

var STEREO = document.getElementById('musicAppAudio'),
		albumArtInterval;


var musicAppCore = {
	// Music Search
	submitMusicSearch: function() {
		// TODO - pass arguments through peram
		var $searchInput = $('#musicSearchInput'),
				searchType = $searchInput.attr('data-searchtype'),
				searchValue = $searchInput.val();

		if (searchType === 'artist') {
			this.getArtistData(searchValue);
		} else {
			// All songs search
			this.pullThisSongToQueuedList(searchValue);
		}

		$('.tt-dropdown-menu').fadeOut();
		$searchInput.blur();
	},
	// Fetch Data
	getArtistData: function(artistName) {
		$.get( "ajax-endpoints/getArtistData.php", { artistName: artistName } )
			.done(function( data ) {
				// show search queued list
				musicAppUi.showSearchPlaylist();

				musicAppCore.constructSearchPlaylist(data, artistName);
			});
	},
	pullArtistList: function($thisElement) {
		// lock button to prevent duplicate clicks
		$thisElement.addClass('active').attr('disabled', 'disabled');

		// show search queued list
		musicAppUi.showSearchPlaylist();

		this.constructSearchPlaylist(musicAppLibrary.ArtistNames, false, true);

		$thisElement.removeClass('active').removeAttr('disabled');
	},
	// Construct HTML
	constructSearchPlaylist: function(JSON, artistName, isArtistList, isPlaylist, playlistName) {
		var playlistHTML = '',
				thisAlbumName = '',
				songName = '';

		for (var i = 0; i < JSON.length; i++) {
			if(typeof JSON[i] === 'string') {

				if (!isArtistList && !isPlaylist) {
					// Album Name
					thisAlbumName = JSON[i];

					playlistHTML += '<li class="albumNameHeading">'+
														thisAlbumName +
														'<span class="hoverHint btn btn-success">Click to play album</span>'+
													'</li>';
				} else if (isArtistList) {
					// Artist list
					artistName = JSON[i];

					playlistHTML += '<li>'+
														'<a class="artistListItem">'+
															'<span class="">'+ artistName +'</span>'+
														'</a>'+
													'</li>';

				} else if (isPlaylist) {
					// Song PLaylist
					thisSongData = parseSongHref(JSON[i], false);

					playlistHTML += buildPlaylistHtml(thisSongData);
				}
			} else {
				// this item is array of songs
				for (var j = 0; j < JSON[i].length; j++) {
					// remove numbers at begining of string
					songName = trimSongName(JSON[i][j]);

					playlistHTML += '<li>'+
														'<a href="music-library/'+ artistName +'/'+ thisAlbumName +'/'+ JSON[i][j] +'">'+
															'<span class="thisSongName">' + songName + '</span>'+
															'<span class="thisArtistName">'+ artistName +'</span>'+
														'</a>'+
													'</li>';
				}
			}
		}

		// Set playlist name in header
		if (!isArtistList && !isPlaylist) {
			$('#playlistHeader h3 .searchPlaylistName').html(artistName);
		} else if (isArtistList) {
			$('#playlistHeader h3 .searchPlaylistName').html('Artist List');
		} else if (isPlaylist) {
			$('#playlistHeader h3 .searchPlaylistName').html(playlistName);
		}

		$('#playlistHeader h3 .playlistNameDash').fadeIn();
		// Inject html into playlist
		$('#searchPlaylist').html(playlistHTML);
	},
	pullThisSongToQueuedList: function(thisSong, isDragDrop) {
		var songData, thisNewSongHTML;

		if (isDragDrop === true) {
			songData = parseSongHref(thisSong, true);
			thisNewSongHTML = buildPlaylistHtml(songData, true);
		} else {
			// show user queued list
			musicAppUi.showQueuedPlaylist();

			songData = parseSongHref(thisSong, false);
			thisNewSongHTML = buildPlaylistHtml(songData);
		}

		$('#currentPlaylist').append(thisNewSongHTML);
		$('#musicSearchInput').typeahead('setQuery', '');
	},
	pullRandomPlaylist: function() {
		var displayLimit = 100,
				thisRandomPlaylist = randomIndexFromArray(musicAppLibrary.AllSongs, displayLimit);

		musicAppUi.showSearchPlaylist();
		this.constructSearchPlaylist(thisRandomPlaylist, false, false, true, 'Random Playlist');
	},

	/* STEREO ACTIONS */
	// Play song
	play: function() {
		STEREO.play();
	},
	// pause song
	pause: function() {
		STEREO.pause();
	},
	// Play/Pause Toggle
	playPauseToggle: function() {
		if (STEREO.paused) {
			// Play
			this.play();
		} else {
			// Pause
			this.pause();
		}
	},
	isPlaying: function() {
		if (STEREO.paused) {
			return false;
		} else {
			return true;
		}
	},
	// Set STEREO src
	setSrc: function(songPath) {
		STEREO.src = songPath;
		STEREO.preload = 'auto';
		STEREO.load();
	},
	// Playing song length
	getPlayingSongLength: function() {
		// Note this probably wont help on play
		// see Audio Element in musicApp.Events.js
		var newSongLength;

		if(isNaN(STEREO.duration)) {
			// STEREO.duration = STEREO.duration;
			var songLengthInterval = setInterval(function() {

				if (!isNaN(STEREO.duration)) {
					clearInterval(songLengthInterval);

					newSongLength = reformatSeconds(STEREO.duration);

					if (STEREO.duration === Infinity) {
						return '<span class="infinity">&#8734;</span>';
					}

					$('.currentSongLength').text(newSongLength);
					return newSongLength;
				}
			}, 250);

		} else {
			// Is a number

			if (STEREO.duration === Infinity) {
				return '<span class="infinity">&#8734;</span>';
			}

			newSongLength = reformatSeconds(STEREO.duration);
			return newSongLength;
		}
	},
	nextSong: function($currentSongElement, playSong, getAlbumArtLate) {
		if ($currentSongElement.next().hasClass('albumNameHeading')) {
			this.songClicked($currentSongElement.next().next());
			return '';
		}

		this.songClicked($currentSongElement.next(), playSong, getAlbumArtLate);
	},
	prevSong: function($currentSongElement) {
		if ($currentSongElement.prev().hasClass('albumNameHeading')) {
			this.songClicked($currentSongElement.prev().prev());
			return '';
		}

		this.songClicked($currentSongElement.prev());
	},
	songClicked: function($thisElement, playSong, getAlbumArtLate) {
		// Detect classes to be ignored
		if ($thisElement.children('a').hasClass('artistListItem')) {
			// This click was on an artist list item - load songs for that artist
			var clickedArtist = $thisElement.children('a').children('span').text();

			this.getArtistData(clickedArtist);
			return '';
		}

		if ($thisElement.children('a').hasClass('ignore')) {
			return '';
		}

		// Pause song if already has class currentSong
		if ($thisElement.hasClass('currentSong')) {
			this.playPauseToggle();

			return '';
		}

		// Preform checking on element to determine it is a song & play song
		if ($thisElement.hasClass('albumNameHeading')) {
			// Advance to next li
			$thisElement.next().click();
			return '';
		}

		// Vars only needed if the above test does not pass
		var thisSongHref = $thisElement.children('a').attr('href'),
				songData = parseSongHref(thisSongHref),
				wasPaused = false;

		if (!songData) {
			// Probably the new song is at the beginning or end of list
			return '';
		}

		if (!this.isPlaying() && $('.currentSong').length > 0) {
			wasPaused = true;
		}

		// update href
		this.setSrc(thisSongHref);

		// STEREO PLAY
		this.play();
		if (wasPaused && !playSong) {this.pause();}

		musicAppUi.setSongDataInUI(songData, $thisElement, getAlbumArtLate);
	},
	seekToThisPosition: function($thisElement, event) {
		var parentOffset = $thisElement.offset(),
				parentWidth = $thisElement.css('width'),
				relX = event.pageX - parentOffset.left,
				positionPercentage,
				secondsToSeekTo;

		parentWidth = parentWidth.replace('px', '');

		positionPercentage = progressBarNewPercentage(parentWidth, relX);
		secondsToSeekTo = parseInt((relX / parentWidth) * STEREO.duration, 10);

		// Update audio element and UI
		STEREO.currentTime = secondsToSeekTo;
		$('.currentPositionBar .position').css('width', positionPercentage + '%');
	},
	// Playlist Actions
	updatePlaylistList: function() {
		// Get updated playlist-list and updateUi
		$.get( "ajax-endpoints/getPlaylists.php")
			.done(function( data ) {
				if (data) {
					musicAppLibrary.playlists = data;
					musicAppCore.initalizePlaylists();
				}
			});
	},
	initalizePlaylists: function() {
		if (typeof musicAppLibrary !== 'undefined' && musicAppLibrary.playlists) {
			var playlistHTML = '';

			for (var i = 0; i < musicAppLibrary.playlists.length; i++) {
				// clean .json from name
				musicAppLibrary.playlists[i] = musicAppLibrary.playlists[i].replace('.json', '');

				playlistHTML += '<li><a class="btn btn-block">' + musicAppLibrary.playlists[i] + '</a></li>';
			}

			$('#savedPlaylistContainer').html(playlistHTML);
		}
	},
	loadThisPlaylist: function($thisElement) {
		var thisPlaylistName = $thisElement.text();

		$thisElement.addClass('active').attr('disabled', 'disabled');

		$.get( "ajax-endpoints/getThisPlaylist.php", { name: thisPlaylistName } )
			.done(function( data ) {
				if (data) {
					var playlistHTML = '';

					data = $.parseJSON(data);

					for (var i = 0; i < data.length; i++) {
						var thisSongData = parseSongHref(data[i], false);

						playlistHTML += buildPlaylistHtml(thisSongData);
					}

					musicAppUi.showQueuedPlaylist();

					$('#playlistHeader h3 .playlistName').html(thisPlaylistName);
					$('#playlistHeader h3 .playlistNameDash').fadeIn();
					$('#currentPlaylist').html(playlistHTML);

					$thisElement.removeClass('active').removeAttr('disabled');
				}
			});
	},
	// Save playlist
	savePlaylist: function() {
		var playlistName = trimString($('.playlistName').text()),
				playlistJSON = [];

		$('#currentPlaylist li').each(function() {
			var thisHref = $(this).children('a').attr('href');

			if (thisHref) {
				thisHref = thisHref.replace('music-library/', '');
				playlistJSON.push(thisHref);
			}
		});

		if (playlistJSON.length > 0) {

			if (playlistName == 'Name Playlist' || playlistName === '') {
				this.setPlaylistName();
				return '';
			}

			$.post( "ajax-endpoints/savePlaylist.php", { name: playlistName, JSON: playlistJSON })
				.done(function( data ) {
					if (data) {
						displayAlert('success', 'Playlist - ' + playlistName + ' has been saved successfully! ', 2000);
						$('.refreshPlaylistList').click();
					}
				});
		}
	},
	// Set playlist name
	setPlaylistName: function(newPlaylistName) {
		if (!newPlaylistName) {
			// Build and show modal
			musicAppModal.build(
				// Header
				'Set Playlist Name',
				// Body
				'<form class="form-horizontal" role="form">' +
					'<div class="form-group">' +
						'<label for="newPlaylistName" class="col-xs-3 control-label">Playlist Name</label>' +
						'<div class="col-xs-8">' +
							'<input type="text" class="form-control" id="newPlaylistName" placeholder="Playlist Name">' +
						'</div>' +
					'</div>' +
				'</form>',
				// Footer
				'<div class="btn btn-primary setPlaylistNameBtn submitForm">Set Playlist Name</div>'
			);

			musicAppModal.show();

			$('.setPlaylistNameBtn').click(function() {
				var thisNewName = $('#newPlaylistName').val();
				musicAppCore.setPlaylistName(thisNewName);
			});
		} else {
			// Set newPlaylistName in UI
			musicAppModal.hide();
			$('.playlistNameDash').fadeIn();
			$('.playlistName').text(newPlaylistName);
		}
	},
	// Clear playlist
	clearPlaylist: function() {
		if (window.confirm("Are you sure you want to clear this playlist?")) {
			$('#currentPlaylist').html('');
			$('.playlistName').html('');
			$('.playlistNameDash').fadeOut();
		}
	},
	// Remove song from playlist
	removeSongFromPlaylist: function($thisElement) {
		$thisElement.parents('li').remove();
	},
	// Add song to user queued playlist
	addSongToPlaylist: function($thisElement) {
		var songToBeAdded = $thisElement.parents('li').html();

		$('#currentPlaylist').append('<li>'+ songToBeAdded +'</li>');
	},
	// Album art
	pullAlbumArt: function(getAlbumArtLate) {
		// prevent redundant album art calls on erroring out files
		if (getAlbumArtLate) {
			var firstSRC = STEREO.src;

			clearTimeout(albumArtInterval);
			albumArtInterval = setTimeout(function() {

				if (firstSRC === STEREO.src) {
					musicAppCore.pullAlbumArt();
				} else {
					musicAppCore.pullAlbumArt(true);
				}
			},500);

			return '';
		}

		// hide container
		$('#albumArtContain').addClass('noAlbumArt');

		var thisSrc = STEREO.src;

		ID3.loadTags(thisSrc, function() {
			var tags = ID3.getAllTags(thisSrc),
					image = tags.picture,
					base64String = "";

			if("picture" in tags ) {

				for (var i = 0; i < image.data.length; i++) {
					base64String += String.fromCharCode(image.data[i]);
				}
				$('.albumArtWell img').attr('src', "data:" + image.format + ";base64," + window.btoa(base64String));

				setTimeout(function() {
					$('#albumArtContain').removeClass('noAlbumArt');
				}, 300);
			} else {
				$('#albumArtContain').addClass('noAlbumArt');
			}
		},
		{tags: ['picture']});

	},
	// Get array of the active playlist
	getActivePlaylistArray: function() {
		var playlistJSON = [];

		$('.activePlaylist li').each(function() {
			var thisHref = $(this).children('a').attr('href');

			if (thisHref) {
				thisHref = thisHref.replace('music-library/', '');
				playlistJSON.push(thisHref);
			}
		});

		if (playlistJSON.length > 0) {
			return playlistJSON;
		} else {
			return [];
		}
	},
	getPlayingSongIndexInPlaylist: function() {
		var indexCount = 0,
				returnIndex,
				currentSongFound = false;

		$('.activePlaylist li').each(function() {
			if ($(this).hasClass('currentSong')) {
				currentSongFound = true;
				returnIndex = indexCount;
			}
			indexCount++;
		});

		if (currentSongFound) {
			return returnIndex;
		} else {
			return false;
		}
	}
};
