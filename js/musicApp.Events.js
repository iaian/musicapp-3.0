/* Music App EVENTS JS
 *  - DOM Event Handlers
 *			Keep this shit lightweight
 *
 *	DEPENDS ON -
 *		- musicApp.Ui.js
 *		- musicApp.Core.js
 *		- HTML & CSS
 *
 *	Event handlers should be listed in their linear order on the page - left to right, top to bottom.
 *
 */

// Global Vars
var pauseInterval,
		STEREO_LAST_SONGLENGTH;


/* TOPBAR */

$('.playPauseBtn').click(function() {
	musicAppCore.playPauseToggle();
});
$('.forwardBtn').click(function() {
	musicAppCore.nextSong($('.currentSong'));
});
$('.backwardBtn').click(function() {
	musicAppCore.prevSong($('.currentSong'));
});

// Detect click position on progressbar
$('.currentPositionBar').mouseup(function(e) {
	musicAppCore.seekToThisPosition($(this), e);
});


/* SIDEBAR */

// Music search display
$('.musicSearchToggle').click(function() {
	musicAppUi.searchDisplayButtonToggle($(this));
});

// Search input and typeahead events
$('#musicSearchForm').submit(function(e) {
	e.preventDefault();
	musicAppCore.submitMusicSearch();
})
	.bind('typeahead:autocompleted typeahead:selected', function() {
		musicAppCore.submitMusicSearch();
		// Allow tab
	});

// Search input - clear on click
$('#musicSearchInput').click(function() {
	$(this).typeahead('setQuery', '');
});

// List artists in search playlist
$('.listArtistsBtn').click(function() {
	musicAppCore.pullArtistList($(this));
});

// Random playlist
$('.randomPlaylistBtn').click(function() {
	musicAppCore.pullRandomPlaylist();
});

// Playlist list display toggle
$('.togglePlaylistDisplay').click(function() {
	musicAppUi.togglePlaylistDisplay($(this));
});

// Playlist click - load playlist
$('#savedPlaylistContainer').delegate('li a', 'click', function(e) {
	musicAppCore.loadThisPlaylist($(this));
});

// Playlist refresh
$('.refreshPlaylistList').click(function() {
	musicAppCore.updatePlaylistList($(this));
});

// Modify Playlist
$('.showPlaylistControlsBtn').click(function() {
	musicAppUi.togglePlaylistControls($(this));
});

// Chat display toggle
$('.toggleChatDisplay').click(function() {
	musicAppUi.toggleChatDisplay($(this));
});

// Show and hide album art
$('#albumArtContain').click(function() {
	musicAppUi.toggleAlbumArtDisplay($(this));
});

/* MAIN COL - PLAYLISTS */

// Toggle main playlist display
$('.togglePlaylistDisplayBtn').click(function() {
	musicAppUi.switchPlaylists($(this));
});

// Playable Song click
$('.songList').delegate('li a', 'click', function(e) {
	e.preventDefault();
	musicAppCore.songClicked($(this).parent());
});

// Album heading click
$('.songList').delegate('li.albumNameHeading', 'click', function(e) {
	e.preventDefault();
	musicAppCore.songClicked($(this).next());
});

// Modify Playlist Btns
// Remove song from playlist
$('.songList').delegate('.removeSongFromPlaylistBtn', 'click', function(e) {
	musicAppCore.removeSongFromPlaylist($(this));
});
// Add song to playlist
$('.songList').delegate('.addSongFromPlaylistBtn', 'click', function(e) {
	musicAppCore.addSongToPlaylist($(this));
});


/* MODAL */
// Hotfix for modal submit
$('#mainModal').delegate('form', 'submit', function(e) {
	e.preventDefault();

	// Submit form
	$('#mainModal .modal-footer .submitForm').click();
});

/* PLAYLIST BOTTOM BUTTONS */
$('.savePlaylistBtn').click(function() {
	musicAppCore.savePlaylist();
});
$('.namePlaylistBtn').click(function() {
	musicAppCore.setPlaylistName();
});
$('.clearPlaylistBtn').click(function() {
	musicAppCore.clearPlaylist();
});

/* AUDIO ELEMENT EVENTS */

$(STEREO).bind('play', function() {
	if (musicAppCore.isPlaying()) {
		$('.playPauseBtn').children('.glyphicon').removeClass('glyphicon-play').addClass('glyphicon-pause');
	} else {
		$('.playPauseBtn').children('.glyphicon').removeClass('glyphicon-pause').addClass('glyphicon-play');
	}

	clearInterval(pauseInterval);
	$('.currentSongTime').removeClass('transparent');
});

$(STEREO).bind('pause', function() {
	if (musicAppCore.isPlaying()) {
		$('.playPauseBtn').children('.glyphicon').removeClass('glyphicon-play').addClass('glyphicon-pause');
	} else {
		$('.playPauseBtn').children('.glyphicon').removeClass('glyphicon-pause').addClass('glyphicon-play');
	}

	pauseInterval = setInterval(function() {
		$('.currentSongTime').toggleClass('transparent');
	}, 500);
});

$(STEREO).bind('timeupdate', function() {
	var currentTime = STEREO.currentTime,
			positionPercentage =  (currentTime / STEREO.duration) * 100;

	$('.currentSongTime').text(reformatSeconds(currentTime));

	if (STEREO.duration === Infinity) {
		positionPercentage =  (currentTime / 220) * 100;
	}

	// Move progress bar
	$('.currentPositionBar .position').css('width', positionPercentage + '%');
});

$(STEREO).bind('durationchange', function() {
	// Goal - after X seconds of STEREO.duration being the same value.. update ui
	durationTimer.restart(STEREO.duration);
});

$(STEREO).bind('progress', function() {
	// Test for Infinity
	if (STEREO.duration === Infinity) {

		$('.currentPositionBar .bufferPosition').css('width', '100%');
		return '';
	}

	if (isNaN(STEREO.duration) || isNaN(STEREO.buffered.end(0))) {
		return '';
	}

	// Show buffered position on bar
	if (STEREO.duration !== STEREO.buffered.end(0)) {
		// Test if song is not fully buffered
		var bufferPositionPercentage =  (STEREO.buffered.end(0) / STEREO.duration) * 100;

		$('.currentPositionBar .bufferPosition').css('width', bufferPositionPercentage + '%');
	} else {
		// Set buffer position to 100%
		$('.currentPositionBar .bufferPosition').css('width', '100%');
	}
});

$(STEREO).bind('ended', function() {
	musicAppCore.nextSong($('.currentSong'), true);
});

$(STEREO).bind('error', function() {
	displayAlert('info', ' <b>Playing next song</b> - This song file cannot be played, probably .aac or .m4a', 5000);
	musicAppCore.nextSong($('.currentSong'), true, true);
});

/* DRAG AND DROP EVENTS */

// Song drag
$('.songList').delegate('li a', 'dragstart', function(event) {
	var thisSongSRC = $(this).attr('href'),
			$draggedElement = $(this),
			dragStartY = event.originalEvent.pageY;

	if (!thisSongSRC || thisSongSRC === '') {
		return '';
	}

	this.style.opacity = '0.7';
	$('.songList li a').unbind('dragover drop');

	// Search playlist only dragging - add to playlist
	if ($(this).parent().parent().is('#searchPlaylist')) {
		$('#playlistHeader').addClass('isDragging');

		$('.draggingMessage').unbind('dragover drop');

		// Add this song to playlist
		$('.draggingMessage').bind('dragover', function(e) {
			e.preventDefault();
		}).bind('drop', function(e) {
			e.preventDefault();
			musicAppCore.pullThisSongToQueuedList(thisSongSRC, true);
		});
	}

	// Re-order playlist
	$('.songList li a').bind('dragover', function(e) {
		e.preventDefault();
	}).bind('drop', function(e) {
		e.preventDefault();
		// Insure user is not dropping element on same song - if so ignore
		if ($(this).attr('href') !== $draggedElement.attr('href')) {
			var dragEndY = e.originalEvent.pageY,
					draggedElementHTML = $draggedElement.parent().html();

			$draggedElement.parent().addClass('removeMe');

			if (dragStartY > dragEndY) {
				// Prepend
				$(this).parent().before('<li>'+ draggedElementHTML +'</li>');
			} else {
				// Append
				$(this).parent().after('<li>'+ draggedElementHTML +'</li>');
			}
		}
	});
});
// Song dragend
$('.songList').delegate('li a', 'dragend', function(e) {
	$('.songList li a').css('opacity', '1');
	$('#playlistHeader').removeClass('isDragging');
	$('.songList li.removeMe').remove();
});
