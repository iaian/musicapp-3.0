/* Music App UI JS
 *  - This code supports UI related JS functionality.
 *      Showing and hiding elements and JS to support action.
 *
 *  DEPENDS ON -
 *    musicApp.Core.js
 *    musicApp.Events.js
 *    HTML & CSS
 *
 */

var musicAppUi = {
  /* Topbar */

  // Topbar track info & controls
  showTopBarTrackData: function() {
    $('#topBarSongInfo, .topBarBtnGroup').fadeIn();
  },
  hideTopBarTrackData: function() {
    $('#topBarSongInfo, .topBarBtnGroup').fadeOut();
  },
  // Search
  searchDisplayButtonToggle: function($thisElement) {
    var newSearchType = $thisElement.attr('data-searchType'),
        // cache commonly used elements
        $musicMainCol = $('#musicApp-Main'),
        $musicSearchInput = $('#musicSearchInput'),
        currentSearchType = $musicSearchInput.attr('data-searchType');

    // Setup search
    $musicSearchInput.attr('data-searchType', newSearchType);

    // Insure last clicked button has active class
    $('.musicSearchToggle').removeClass('active');
    $thisElement.addClass('active');

    if (newSearchType === 'all') {
      $musicSearchInput.attr('placeholder', 'Search All');

      // Setup typeahead
      $musicSearchInput.typeahead('destroy');

      $musicSearchInput.typeahead({
        name: 'All Songs',
        local: musicAppLibrary.AllSongs,
        limit: 10,
        header: '<span class="autoCompleteHeader">Tab, Click, or Arrow key to a song name below to add this song to your user queued list.</span>'
      });
    } else {
      $musicSearchInput.attr('placeholder', 'Search Artist');

      // Setup typeahead
      $musicSearchInput.typeahead('destroy');

      $musicSearchInput.typeahead({
        name: 'Artist',
        local: musicAppLibrary.ArtistNames,
        limit: 10,
        header: '<span class="autoCompleteHeader">Tab, click, or Arrow key to an artist name below to see list of all stored albums, and songs.</span>'
      });
    }

    // Display search logic
    if ($musicMainCol.hasClass('searchShown')) {

      if (newSearchType === currentSearchType) {
        $thisElement.removeClass('active');
        $musicMainCol.removeClass('searchShown');

        // Prevents $musicSearchInput focus
        return '';
      }
    } else {
      $musicMainCol.addClass('searchShown');
    }
    $musicSearchInput.typeahead('setQuery', '').focus();
  },
  hideSearch: function() {
    // Yeah this is lazy .. or awesome
    $('.musicSearchToggle.active').click();
  },
  // Playlist list
  togglePlaylistDisplay: function($thisElement) {
    $thisElement
      .toggleClass('active')
      .parent().toggleClass('active');
  },
  // Chat
  toggleChatDisplay: function($thisElement) {
    $thisElement
      .toggleClass('active')
      .removeClass('newChat');

    if ($thisElement.hasClass('active')) {
      // Show chat
      $('#musicApp-Main').addClass('chatShown');
      $thisElement.html('Hide Chat <span class="glyphicon glyphicon-comment"></span>');

      setTimeout(function() {
        $('#messageInput').focus();
      }, 500);
    } else {
      // Hide chat
      $('#messageInput').blur();
      $('#musicApp-Main').removeClass('chatShown');
      $thisElement.html('Show Chat <span class="glyphicon glyphicon-comment"></span>');
    }
  },
  // Album art display
  toggleAlbumArtDisplay: function($thisElement) {
    var $albumArtContainer = $thisElement,
        $albumArtHeader = $('.albumArtHeader');

    $albumArtContainer.toggleClass('slideDown');

    if ($albumArtContainer.hasClass('slideDown')) {
      // Show album art message
      $albumArtHeader.html('Show Album Art<span class="glyphicon glyphicon-chevron-up"></span>');
    } else {
      // Hide album art message
      $albumArtHeader.html('Hide Album Art<span class="glyphicon glyphicon-chevron-down"></span>');
    }
  },
  // Switch Playlists
  switchPlaylists: function($thisElement) {
    var showId = $thisElement.attr('data-showId'),
        hideId = $thisElement.attr('data-hideId');

    if (showId === '#currentPlaylist') {
      // Search Playlist
      $('#musicApp-Main').removeClass('searchPlaylist').addClass('userPlaylist');

      // Show current pl
      $thisElement.text('Show Search Playlist');
      $('#playlistHeader h3 .playlistLabel').text('User Queued Playlist');
    } else {
      // User playlist
      $('#musicApp-Main').removeClass('userPlaylist').addClass('searchPlaylist');

      // Show hidden pl
      $thisElement.text('Show User Queued Playlist');
      $('#playlistHeader h3 .playlistLabel').text('Search Playlist');
    }

    $(hideId).addClass('listHide').removeClass('activePlaylist');
    $(showId).removeClass('listHide').addClass('activePlaylist');

    $thisElement.attr('data-showId', hideId).attr('data-hideId', showId);
  },
  // used to show only the qued playlist
  showQueuedPlaylist: function() {
    if ($('.togglePlaylistDisplayBtn').attr('data-showId') === '#currentPlaylist') {
      $('.togglePlaylistDisplayBtn').click();
    }
  },
  showSearchPlaylist: function() {
    if ($('.togglePlaylistDisplayBtn').attr('data-showId') !== '#currentPlaylist') {
      $('.togglePlaylistDisplayBtn').click();
    }
  },
  // Playlist Actions
  setSongDataInUI: function(songData, $thisElement, getAlbumArtLate) {
    // update page title
    document.title = songData.songName + ' - ' + songData.artistName;

    // Reset timers and progress bars
    $('.currentSongTime, .currentSongLength').html('0:00');
    $('.currentPositionBar .position, .currentPositionBar .bufferPosition').css('width', '0px');

    // Update the topbar
    $('.currentArtistName').html(songData.artistName);
    $('.currentAlbumName').html(songData.albumName);
    $('.currentSongName').html(songData.songName);

    $('.currentSongLength').html(musicAppCore.getPlayingSongLength());

    // Add active class to this song element
    $('.currentSong').removeClass('currentSong');
    $thisElement.addClass('currentSong');

    // Scroll to current song
    this.scrollToPlayingSong();

    // Show topbar
    this.showTopBarTrackData();

    // Get album art
    musicAppCore.pullAlbumArt(getAlbumArtLate);
  },
  playlistScroll: function(pixelsToScroll, scrollSpeed) {
    if (!scrollSpeed) {
      scrollSpeed = 700; // ms
    }

    $('#playlistScrollContain').animate({
      scrollTop: pixelsToScroll
    }, scrollSpeed);

  },
  scrollToPlayingSong: function() {
    var scrollToPixel = 0;

    $('.activePlaylist li').each(function() {
      if ($(this).hasClass('currentSong')) {
        return false;
      }
      scrollToPixel = scrollToPixel + $(this).outerHeight();
    });

    if (scrollToPixel !== 0) {
      this.playlistScroll(scrollToPixel, 600);
    }
  },
  togglePlaylistControls: function($thisElement) {
    $thisElement.toggleClass('active');

    if ($thisElement.hasClass('active')) {
      // Show controls
      $('.songList').addClass('controlsShown');
    } else {
      // Hide controls
      $('.songList').removeClass('controlsShown');
    }
  }
};
