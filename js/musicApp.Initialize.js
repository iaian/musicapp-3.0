/* Music App Initialize JS
 *  -  Code related to document ready or initializing the app.
 *
 */

$( document ).ready(function() {
  musicAppCore.initalizePlaylists();

});