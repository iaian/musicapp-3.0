/* Music App Modal
 *  -  An object for manipulating the modal
 *
 *  NOTE - if you need a form to submit apply class : submitForm to the button
 *
 */

var $mainModal = $('#mainModal');

var musicAppModal = {
  show: function() {
    $mainModal.modal('show');

    setTimeout(function() {
      $('#mainModal .modal-body input:first-of-type').focus();
    }, 250);
  },
  hide: function() {
    $mainModal.modal('hide');
  },
  build: function(header, body, footer) {
    $mainModal.find('.modal-header .modal-title').html(header);
    $mainModal.find('.modal-body').html(body);
    $mainModal.find('.modal-footer .content').html(footer);
  }
};