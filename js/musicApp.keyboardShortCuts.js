/* MusicApp Keyboard Shortcuts
 *
 */

var keydownAllowed = true;

$(document).keydown(function(e) {
	// Insure that we dont get repeating key events
	if (!keydownAllowed) { return '';}
	keydownAllowed = false;

	// DEBUG
	// console.log(e.which);

	// Detect focus on a btn and remove focus
	if ($(document.activeElement).hasClass('btn')) {
		$(document.activeElement).blur();
	}

	// Prevent from screwing with inputs and other text fields
	if ($(document.activeElement).is('input, textarea')) {
		return '';
	}

	// Play & pause - spacebar
	if (e.which === 32) {
		e.preventDefault();

		if ($('.activePlaylist .currentSong').length === 0) {
			// Detects no current song and plays starting at the top of showing playlist
			$('.activePlaylist li:first-child').click();

			return '';
		}

		musicAppCore.playPauseToggle();
	}

	// Forwarad a song - down arrow + right arrow
	if (e.which === 39 || e.which === 40) {
		e.preventDefault();
		$('.forwardBtn').click();
	}

	// Backwards a song - up arrow + left arrow
	if (e.which === 37 || e.which === 38) {
		e.preventDefault();
		$('.backwardBtn').click();
	}

	// Search inputs display - tab
	if (e.which === 9) {
		e.preventDefault();
	}

/*
	// Increase volume - plus
	if (e.which === 61 || e.which === 107) {
		bumpVolume('10');
	}
	// Decrease volume - minus
	if (e.which === 173 || e.which === 109) {
		bumpVolume('-10');
	}
*/
}).keyup(function(e) {
	keydownAllowed = true;
});

// Selects first item on tab
$('#musicSearchInput').keydown(function(e) {
	// Tab
	if(e.which === 9) {
		e.preventDefault();
		$('.tt-suggestions .tt-suggestion:first-child').addClass('tt-is-under-cursor');
	}
});
