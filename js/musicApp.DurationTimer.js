/* -- Duration Timer --
 *		This is used to insure we have an accurate duration time displayed in the song length
 *		- needed because when the duration changes as more of the song is loaded.
 *
 */

var STEREO_DURATION_TIMER,
		STEREO_DURATION_INTERVAL,
		STEREO_TIMER_LENGTH = 10000, // In ms
		STEREO_firstSongDuration,
		STEREO_timerRunning = false;

var durationTimer = {
	setTimerLength: function(timerLength) {
		STEREO_TIMER_LENGTH = timerLength;
		durationTimer.clearTimers();
	},
	restart: function(currentDuration) {
		if (STEREO.duration === Infinity) {
			durationTimer.clearTimers();

			$('.currentSongLength').html('<span class="infinity">&#8734;</span>');
			return '';
		}

		if (!STEREO_timerRunning) {
			durationTimer.clearTimers();

			var newCurrentDuration = currentDuration;

			STEREO_firstSongDuration = newCurrentDuration;
			STEREO_timerRunning = true;

			while(isNaN(STEREO_firstSongDuration)) {
				STEREO_firstSongDuration = STEREO.duration;
			}

			STEREO_DURATION_INTERVAL = setInterval(function() {
				currentDuration = STEREO.duration;
			}, 1000); // 1s

			// If this timeout is allowed to complete, duration should be accurate
			STEREO_DURATION_TIMER = setTimeout(function () {

				if (STEREO.duration === Infinity) {
					durationTimer.clearTimers();

					$('.currentSongLength').html('<span class="infinity">&#8734;</span>');
					return '';
				}

				STEREO_timerRunning = false;

				STEREO_firstSongDuration = Math.round(STEREO_firstSongDuration);
				newCurrentDuration = Math.round(newCurrentDuration);

				if (STEREO_firstSongDuration === newCurrentDuration) {
					// Duration is stable
					$('.currentSongLength').text(musicAppCore.getPlayingSongLength());

					durationTimer.clearTimers();

					// Debug
					// console.log('stableDuration - ' + reformatSeconds(STEREO_firstSongDuration));
				} else {
					// Not stable duration try again
					durationTimer.restart();

					// Debug
					// console.log('not stable - '+ reformatSeconds(STEREO_firstSongDuration) +' !== '+ reformatSeconds(STEREO_firstSongDuration));
				}
			}, STEREO_TIMER_LENGTH);

		}
	},
	clearTimers: function() {
		if (!STEREO_timerRunning) {
			clearTimeout(STEREO_DURATION_TIMER);
			clearInterval(STEREO_DURATION_INTERVAL);
		}
	},
};