/* This contains all the JS code related to firebase
 *    - Tests PHP set var ENABLE_FIREBASE before loading any firebase related code
 *
 *    Useful vars :
 *
 *      ENABLE_FIREBASE - boolean value from the php config
 *      FIREBASE_URL - url used to connect to firebase
 *
 *    TODO :
 *      Hide all firebase related UI elements from the page if bool - ENABLE_FIREBASE is true.
 *
 */

// GLOBAL VARS
var imRecievedSoundPath = 'sounds/bcfire01.wav',
    activeUsers = {},
    sharedPlaylistUsers,
    thisUserSharedPlaylist,
    canPlaySound = false,
    sharePlaylist = false,
    chatAudioElement,
    sharingInterval,
    // Firebase refrences
    messagesRef,
    myUserRef,
    playlistRef,
    myUserPlaylistRef;


// Only load firebase stuffs if ENABLE_FIREBASE is defined and equal to 1 - which is set in PHP config file
if (typeof ENABLE_FIREBASE !== 'undefined' && ENABLE_FIREBASE === "1") {
  // Add firebase to script to page
  $.getScript( "https://cdn.firebase.com/js/client/1.0.3/firebase.js", function() {
    initailizeWebChat();
    initializeUserPersistance();
    initializePlaylistSharing();
  });

  // Inject chat audio element into DOM
  $('body').append('<audio id="chatAudio"></audio>');
  chatAudioElement = document.getElementById('chatAudio');

} // ENABLE_FIREBASE === 1


/* FUNCTIONS */

function initializePlaylistSharing() {
  if (window.localStorage) {
    // First check local storage for preset value
    // This is needed to insure that playlistShare is boolean instead of string
    var sharePlaylistLocalStorage = (localStorage.getItem('playlistShare') === "true");

    if (sharePlaylistLocalStorage === null) {
      sharePlaylistLocalStorage = false;
    }

    // Set the checkbox
    $('#shareCheckbox').prop('checked', sharePlaylistLocalStorage);

    if (sharePlaylistLocalStorage) {
      activateSharingFirebase();
    }
  }

  $('#shareCheckbox').change(function() {
    sharePlaylist = $(this).is(':checked');

    if (window.localStorage) {
      localStorage.setItem('playlistShare', sharePlaylist);
    }

    if (sharePlaylist) {
      activateSharingFirebase();
    } else {
      deActivateSharingFirebase();
    }
  });
}

function activateSharingFirebase() {
  playlistRef = new Firebase(FIREBASE_URL + "/playlistSharing");
  myUserPlaylistRef = new Firebase(FIREBASE_URL + "/playlistSharing/" + userName);

  myUserPlaylistRef.set(updateSharedPlaylist(true));
  myUserPlaylistRef.onDisconnect().remove();

  // Interval to keep playlist up to date
  sharingInterval = setInterval(function() {
    updateSharedPlaylist();
  }, 2000); // 2 seconds

  playlistRef.on('value', function (snapshot) {
    updateSharedUserArray(snapshot.val());
  });
  playlistRef.on('child_removed', function (snapshot) {
    updateSharedUserArray(snapshot.val());
  });
}

function updateSharedUserArray(thisSharedPlaylists) {
  // Insure object is not null
  if (thisSharedPlaylists) {

    sharedPlaylistUsers = [];

    for(var prop in thisSharedPlaylists) {
      var thisPlaylist = thisSharedPlaylists[prop],
          playlistRefExpireTime = new Date();

      playlistRefExpireTime.setDate(playlistRefExpireTime.getDate()-1);
      playlistRefExpireTime = playlistRefExpireTime.getTime();

      if (thisPlaylist.lastModified < playlistRefExpireTime) {
        // More than a day old
        // This needs to be removed from firebase
        playlistRef.child(prop).remove();
        return;
      }

      // Only add this user to sharedPlaylistUsers array if has playlist
      if (
        thisPlaylist.playlist &&
        thisPlaylist.playlist.length > 0
      ) {
        sharedPlaylistUsers.push({
          name: thisPlaylist.username,
          shareDataRef: prop
        });
      }
    }
    updateSharedPlaylistButtons();
  }
}

function updateSharedPlaylistButtons() {
  $('.activeUser').each(function() {
    var thisName = $(this).text(),
        thisColor = $(this).css('color');

    for(var i in sharedPlaylistUsers) {
      if(
        sharedPlaylistUsers[i]['name'] == thisName &&
        thisName !== userName
      ) {
        // prevent duplicate click handlers
        if ($(this).parent().hasClass('hasSharedPlaylist')) {
          return;
        }

        var thisPlaylistRef = sharedPlaylistUsers[i]['shareDataRef'];

        $(this).parent()
          .addClass('hasSharedPlaylist')
          .attr('data-toggle', 'tooltip')
          .attr('title',"Click to sync <b class='whiteOutlineTxt' style='color: "+ thisColor +"'>"+ thisName +"'s</b> playlist to yours")
          .attr('data-shareDataRef', thisPlaylistRef)
          .tooltip({
            trigger: 'hover',
            html: true
          })
          .click(function() {
            if (window.confirm("Click ok to sync with "+ thisName +"'s playlist\n\nNote : This will overwrite your current playlist")) {
              var syncedUsersPlaylistRef = playlistRef.child(thisPlaylistRef);

              syncedUsersPlaylistRef.once('value', function (snapshot) {
                var newPlaylistData = snapshot.val(),
                    playlistHTML = '';

                for (var i = 0; i < newPlaylistData.playlist.length; i++) {
                  var thisSongData = parseSongHref(newPlaylistData.playlist[i], false);

                  playlistHTML += buildPlaylistHtml(thisSongData);
                }

                musicAppUi.showSearchPlaylist();

                $('#playlistHeader h3 .searchPlaylistName').html(" synced from " + newPlaylistData.username);
                $('#searchPlaylist').html(playlistHTML);
              });
            }
          });
        } else {
          // Remove sharing handlers
          $(this).parent()
            .removeClass('hasSharedPlaylist')
            .removeAttr('data-toggle')
            .removeAttr('title')
            .tooltip('destroy')
            .unbind('click');
        }
      }
  });
}

function updateSharedPlaylist(returnObject) {
  var now = Date.now();

  // local version of the this users sharedPlaylist object, which will be stored in firebase
  thisUserSharedPlaylist = {
                          username: userName,
                          playlist: musicAppCore.getActivePlaylistArray(),
                          currentSong: musicAppCore.getPlayingSongIndexInPlaylist(),
                          currentDuration: STEREO.currentTime,
                          lastModified: now
                        };

  if (returnObject) {
    return thisUserSharedPlaylist;
  }

  // The purpose of this is to only update firebase when needed,
  //   since this function is used by an setInterval()
  myUserPlaylistRef.once('value', function (snapshot) {
    var firebaseSharedPl = snapshot.val();

    if (firebaseSharedPl === null) {
      myUserPlaylistRef.set(thisUserSharedPlaylist);
      return;
    }

    if (!firebaseSharedPl.playlist) {
      firebaseSharedPl.playlist = [];
    }

    // This is the test that determines if firebase gets updated
    if  (
      firebaseSharedPl.playlist.length != thisUserSharedPlaylist.playlist.length ||
      firebaseSharedPl.playlist[0] != thisUserSharedPlaylist.playlist[0] ||
      firebaseSharedPl.currentSong != thisUserSharedPlaylist.currentSong ||
      firebaseSharedPl.currentDuration != thisUserSharedPlaylist.currentDuration
    ) {
      myUserPlaylistRef.set(thisUserSharedPlaylist);
    }
  });
}

function deActivateSharingFirebase() {
  playlistRef.off('value');
  myUserPlaylistRef.remove();

  clearInterval(sharingInterval);
}

function initailizeWebChat() {
  messagesRef = new Firebase(FIREBASE_URL + '/webchat');

  // When the user presses enter on the message input, write the message to firebase.
  $('#messageInput').keypress(function (e) {
    if (e.keyCode == 13) {
      sendMessage();
    }
  });
  // for send message btn
  $('#sendChatMessage').click(function(event) {
    event.preventDefault();
    sendMessage();
  });

  // Add a callback that is triggered for each chat message.
  messagesRef.limit(20).on('child_added', function (snapshot) {
    var message = snapshot.val();

    // Prevent online users json from showing up in chat
    if (typeof message.name === "undefined") {
      return '';
    }

    $('<div/>')
      .text(message.text)
      .prepend($('<span class="name" data-username="'+  message.name +'"/>').text( message.name +': '))
      .appendTo($('#messagesDiv'));

    $('#messagesDiv')[0].scrollTop = $('#messagesDiv')[0].scrollHeight;

    // Insure new messages are marked
    markUsersNames();
    imRecievedSound();
  });
}

function initializeUserPersistance() {
  // Prompt the user for a name to use.
  var name = userName,
      currentStatus = "★ online";

  // Get a reference to the presence data in Firebase.
  var userListRef = new Firebase(FIREBASE_URL + "/onlineUsers");

  // Generate a reference to a new location for my user with push.
  myUserRef = userListRef.push();

  // Get a reference to my own presence status.
  var connectedRef = new Firebase(FIREBASE_URL + "/.info/connected");
  connectedRef.on("value", function(isOnline) {
    if (isOnline.val()) {
      // If we lose our internet connection, we want ourselves removed from the list.
      myUserRef.onDisconnect().remove();

      // Set our initial online status.
      setUserStatus("★ online");
    } else {

      // We need to catch anytime we are marked as offline and then set the correct status. We
      // could be marked as offline 1) on page load or 2) when we lose our internet connection
      // temporarily.
      setUserStatus(currentStatus);
    }
  });

  function getMessageId(snapshot) {
    return snapshot.name().replace(/[^a-z0-9\-\_]/gi,'');
  }

  // Update our GUI to show someone"s online status.
  userListRef.on("child_added", function(snapshot) {
    var user = snapshot.val(),
        newColorObject = getRandomColor(true);

    while (!checkForDupeHue(newColorObject.hue)) {
      newColorObject = getRandomColor(true);
    }

    activeUsers[user.name.toLowerCase()] = {
      "name": user.name.toLowerCase(),
      "color": newColorObject.color,
      "hue": newColorObject.hue
    };

    $("<div/>")
      .attr("id", getMessageId(snapshot))
      .html('<span class="activeUser">' + user.name + "</span> is currently " + user.status)
      .appendTo(".onlineUsersList");

    markUsersNames();
  });

  // Update our GUI to remove the status of a user who has left.
  userListRef.on("child_removed", function(snapshot) {
    $(".onlineUsersList").children("#" + getMessageId(snapshot))
      .remove();
  });

  // Update our GUI to change a user"s status.
  userListRef.on("child_changed", function(snapshot) {
    var user = snapshot.val();
    $(".onlineUsersList").children("#" + getMessageId(snapshot))
      .html('<span class="activeUser">' + user.name + "</span> is currently " + user.status);

    markUsersNames();
  });

  // Use idle/away/back events created by idle.js to update our status information.
  document.onIdle = function () {
    setUserStatus("☆ idle");
  };
  document.onAway = function () {
    setUserStatus("☄ away");
  };
  document.onBack = function (isIdle, isAway) {
    setUserStatus("★ online");
  };

  // User is typing - this is kinda shitty
  $('#messageInput').keyup(function(event) {
    var message = $(this).val();

    // Trim whitespace from begining and end of string
    message.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if(message.length > 0) {
      setUserStatus("Typing..");

      setTimeout(function() {
        setUserStatus("★ online");
      }, 10000);
    } else {
      setUserStatus("★ online");
    }
  });

  setIdleTimeout(15000); // 15 seconds
  setAwayTimeout(60000); // 1 min
} // End initalize user presence

// A helper function to let us set our own state.
function setUserStatus(status) {
  // Set our status in the list of online users.
  myUserRef.update({ name: userName, status: status });
}

function sendMessage() {
  var name = userName;
  var text = $('#messageInput').val();

  if (text.length > 0) {
    messagesRef.push({name:name, text:text});
    $('#messageInput').val('');
    setUserStatus("★ online");
  }
}

function scrollToBottomOfChat() {
  var $messagesDivChildren = $('#messagesDiv div'),
      numberOfMessages = $messagesDivChildren.length;

  if (numberOfMessages === 0) {
    // Wait for messages to appear
    var messagesInterval = setInterval(function() {
      if ($('#messagesDiv div').length > 0) {
        // Messages have now arived
        clearInterval(messagesInterval);
        calculateAndScroll();
        markUsersNames();
      }
    }, 100);
  } else {
    calculateAndScroll();
  }

  function calculateAndScroll() {
    var messageHeight = $('#messagesDiv div').height(),
        numberOfMessages = $('#messagesDiv div').length,
        endScrollPosition = messageHeight * numberOfMessages;

    $('#messagesDiv').animate({
      scrollTop: endScrollPosition + 'px'
    }, 400);
  }
}

function imRecievedSound() {
  // This sound should only fire if someone other than current user sends a message

  // Test last row for existance of username
  var lastMessageUsername = $('#messagesDiv div:last span.name').attr('data-username');

  if (canPlaySound && lastMessageUsername !== userName) {
    chatAudioElement.src = imRecievedSoundPath;
    chatAudioElement.play();

    if (!$('.toggleChatDisplay').hasClass('active')) {
      $('.toggleChatDisplay').addClass('newChat');
    }
  }

  // To prevent first loaded messages from making sounds
  if (!canPlaySound) {
    setTimeout(function() {
      canPlaySound = true;
    }, 4000);
  }
}

function markUsersNames() {
  // Iterate through activeUsers object to find what color should be for this usersname
  $('#messagesDiv div .name, .activeUser').each(function() {
    var thisUserName = $(this).text().toLowerCase(),
        newColor;

    thisUserName = thisUserName.replace(': ', '');

    // Detect missing object and fix
    if (typeof activeUsers[thisUserName] === 'undefined') {
      var newColorObject = getRandomColor(true);

      while (!checkForDupeHue(newColorObject.hue)) {
        newColorObject = getRandomColor(true);
      }

      activeUsers[thisUserName] = {
        "name": thisUserName.toLowerCase(),
        "color": newColorObject.color,
        "hue": newColorObject.hue
      };
    }

    newColor = activeUsers[thisUserName]['color'];

    // No color yet - get one
    if (!newColor) {
      newColor = getRandomColor(true);

      while (!checkForDupeHue(newColor.hue)) {
        newColor = getRandomColor(true);
      }

      activeUsers[thisUserName]['color'] = newColor.color;
      activeUsers[thisUserName]['hue'] = newColor.hue;
    }

    $(this).attr('style', 'color: '+ newColor +';');

    // Bold username of users that do not match js variable - username
    if (thisUserName !== userName.toLowerCase()) {
      $(this).addClass('bold');
    }
  });
}

function checkForDupeHue(thisHue) {
  var returnResult = true;

  $.each(activeUsers, function(index, val) {

    var hueVariance = 15,
        thisMaxValue = activeUsers[index]['hue'] + hueVariance;
        thisMinValue = activeUsers[index]['hue'] - hueVariance;

    if (thisHue > thisMaxValue) {
      // console.log('good hue value');
    } else if (thisHue < thisMinValue) {
      // console.log('good hue value');
    } else {
      returnResult = false;
    }
  });

  // Return boolean
  return returnResult;
}

function getRandomColor(returnHueBool) {
  // Note this function only gets a random hue - saturation and lightness are hard coded
  var randomHue = Math.floor(Math.random() * (360 - 0)) + 0;

  // For returning HUE
  if (returnHueBool) {
    return { "color": "hsl("+ randomHue +", 75%, 35%)", "hue": randomHue };
  }

  return 'hsl('+ randomHue +', 75%, 35%)';
}
