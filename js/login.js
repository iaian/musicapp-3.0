$( document ).ready(function() {

	// Test for localStorage support
	if (window.localStorage) {
		// Retrieve localstorage for set name - if found set
		var storedUsername = localStorage.getItem('name');

		if (storedUsername !== null) {
			$('#login-Name').val(storedUsername);
		}
	}

	// Login form
	$('#loginForm').submit(function(event) {
		event.preventDefault();

		var pw = $('#login-PW').val(),
				name = $('#login-Name').val();

		if (pw === '' || name === '') {
			return '';
		}

		// Set name in localstorage for autofilling in the future
		if (window.localStorage) {
			localStorage.setItem('name', name);
		}

		$.post( "ajax-endpoints/login.php", { name: name, pw: pw })
			.done(function( data ) {
				if (data === 'success') {
					// Logged in
					displayAlert('success', 'Login Successful', 5000);

					// Reload page to arrive at music app
					setTimeout(function() {
						window.location = '';
					}, 600);
				} else {
					// Not logged in
					displayAlert('danger', 'Incorrect Password', 5000);
					$('#login-PW').val('').focus();
				}
			});
	});

});