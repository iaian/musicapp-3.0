/* FUNCTIONS
 *  - Only add functions to this file if they are
 *    used frequently in many parts of the application
 */

function displayAlert(type, message, duration) {
  /*  Type - success, info, warning, danger
   *  Message - any html
   *  Duration - time in MS to display alert for
   *
   *  Example:
   *    displayAlert('danger', 'some html', 5000);
   */

  // Scroll to top of page
  $("html, body").animate({ scrollTop: 0 }, "slow");

  $('#mainAlert')
    .removeClass()
    .addClass('alert alert-dismissable alert-' + type)
    .html('<button type="button" class="close" aria-hidden="true">&times;</button>' +
            message)
    .slideDown();

  // Hide on click
  $('#mainAlert, #mainAlert button.close').click(function() {
    $(this).slideUp();
  });

  if (duration) {
    setTimeout(function() {
      $('#mainAlert').slideUp();
    }, duration);
  }
}

function buildPlaylistHtml(thisSongData, songHasLibPath) {
  var returnHtml ='';

  if (songHasLibPath) {
    returnHtml = '<li>'+
                        '<a href="'+ thisSongData.songPath +'">'+
                          '<span class="thisSongName">' + thisSongData.songName + '</span>'+
                          '<span class="thisArtistName">'+ thisSongData.artistName +'</span>'+
                        '</a>'+
                        '<div class="btn-group songButtonGroup">'+
                          '<div class="btn btn-success addSongFromPlaylistBtn" title="Add song to playlist">+</div>'+
                          '<div class="btn btn-danger removeSongFromPlaylistBtn" title="Remove song from list">-</div>'+
                        '</div>'+
                      '</li>';

  } else {
    returnHtml = '<li>'+
                        '<a href="music-library/'+ thisSongData.songPath +'">'+
                          '<span class="thisSongName">' + thisSongData.songName + '</span>'+
                          '<span class="thisArtistName">'+ thisSongData.artistName +'</span>'+
                        '</a>'+
                        '<div class="btn-group songButtonGroup">'+
                          '<div class="btn btn-success addSongFromPlaylistBtn" title="Add song to playlist">+</div>'+
                          '<div class="btn btn-danger removeSongFromPlaylistBtn" title="Remove song from list">-</div>'+
                        '</div>'+
                      '</li>';

  }

  return returnHtml;
}

function trimSongName(songName) {
  var fixedSongName_Number = songName.replace(/^[0-9]{2}[.\s%20-]*/, ''),
      fixedSongName_FileExt = fixedSongName_Number.replace(/.[a-zA-Z0-9]{3,4}$/, '');

  return fixedSongName_FileExt;
}

function parseSongHref(songHref, hasMusicLibInPath) {
  if (!songHref) {
    return null;
  }

  if (typeof hasMusicLibInPath === 'undefined') {
    hasMusicLibInPath = true;
  }

  var hrefArray, thisArtistName, thisAlbumName, thisSongName, thisSongFileName;

  if (hasMusicLibInPath) {
    hrefArray = songHref.split('/');
    // Skip one for the music directory
    thisArtistName = hrefArray[1];
    thisAlbumName = hrefArray[2];
    thisSongFileName = hrefArray[3];
    thisSongName = trimSongName(hrefArray[3]);
  } else {
    hrefArray = songHref.split('/');
    // Skip one for the music directory
    thisArtistName = hrefArray[0];
    thisAlbumName = hrefArray[1];
    thisSongFileName = hrefArray[2];
    thisSongName = trimSongName(hrefArray[2]);
  }

  return {
    songPath : songHref,
    artistName : thisArtistName,
    albumName : thisAlbumName,
    songName : thisSongName,
    songFileName : thisSongFileName
  };
}

function reformatSeconds(seconds) {
  seconds = Math.round(seconds);

  var minutes = Math.floor(seconds / 60);
  var secondsToReturn = seconds - minutes * 60;

  // Integer time
  minutes = parseInt(minutes, 10);
  secondsToReturn = parseInt(secondsToReturn, 10);

  // For styling
  if (secondsToReturn < 10) {
    secondsToReturn = '0' + secondsToReturn;
  }
  return minutes + ':' + secondsToReturn;
}

function randomIndexFromArray(myArray, nb_picks) {
  for (i = myArray.length-1; i > 1  ; i--) {
    var r = Math.floor(Math.random()*i),
        t = myArray[i];

    myArray[i] = myArray[r];
    myArray[r] = t;
  }

  return myArray.slice(0,nb_picks);
}

function progressBarNewPercentage(parentWidth, newPosition) {
  returnPercentage = (newPosition / parentWidth) * 100;

  if (returnPercentage > 100) {
    returnPercentage = 100;
  }
  return returnPercentage;
}

function trimString(str) {
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

function in_array(needle, haystack) {
    for(var i in haystack) {
        if(haystack[i] == needle) return true;
    }
    return false;
}