# Music App 3.0

This is the third (major) iteration on the music app idea. Faster, stronger, better, and more organized.


## Goals

* Better organization of ajax endpoints, separate file for each endpoint instead of bundling all the actions under one file.
* One big json data for loading data - all songs, all artists, and userplaylists. Can then iterate over json to assign pieces of JSON to specific variables. This allows for requesting a section of json to update the var used for that section.
* Build Javascript in a consistent and object oriented manner.
* Lighter page weight than musicApp-lt (with ajax responses - ~ 567 kb @ 15 requests)


## Layout

For this version the layout will be a topbar, a fixed width sidebar ( left ), and a single main section used to display playlists.
Playlists and queued items will be loaded into a alternate main col that will be able to have its display toggled or overlaid across the main col.

Topbar - fluid width : search artist & search all buttons & status bar song info ( similar to musicApp-lt )

Left vertical col - has a fixed width, contains buttons to control music playing and other common app functions

Main col - fluid width, loads displayed

Queue col - fluid width, identical to main col, loads hidden


## Playlist Interaction

* Random playlist - will load a random assortment of songs into the main col
* List artists - will load list of artists into the main col - on artist click all of that artists albums will be loaded into the main col
* Search Artist - all of that artists albums will be loaded into the main col
* Search All - will load this single song into the hidden queue col
* Playlist - will empty the queue col then load that playlist into queue col
* Click on song from webchat - loads that song into queue col


## Ideas

* When user logs on they must choose a nickname. Next we will use firebase to create a webchat, in webchat we will post a message anytime a user changes songs. If a user clicks a song it will be added to a play queue.

## Project Structure

#### PHP
The password is set in /config/config.php and /AppConfig.php this is what allows access to the site. See example /AppConfig.example.php for what is expected in /AppConfig.php

My main goal with this app was to have as light of a PHP codebase as possible and rely on javascript to insure that the only page load will be after the login screen. This application does not need or use a server side database.

Firebase is being used both for webchat and submitting what song you are listening to into webchat to other users in order to help share songs. The javascript api is built to easily enable or disable and all firebase related modules see /config/appSettings.php (Also the firebase url is set in that file).

Ajax requests endpoints should all be located in the /ajax-endpoints directory. Each distinct data return or object getter or setter should be its *own* .php file. All data is to be transmitted as JSON.

#### JS
All data needed at page load will be placed into the dom, on page load via PHP.

Objects will be able to be updated as a whole or in smaller / more specific pieces.


-----------------------------

# How to Run & Develop

Basically all you need to run this app is PHP and a working web root, and a folder structure like : `/music-library/Artist-Name/Artist-Album/SongName.mp3`. This app should work with any sound file supported by the html audio element.

1. Setup webroot - all you really need is PHP 5.x and a way to get to the directory in a browser
2. `$ git clone git@bitbucket.org:iaian/musicapp-3.0.git`
3. Symbolic link your itunes music folder to the /music-library `$ ln -s ~/Music/iTunes/iTunes\ Media/Music/* ./music-library/ ` ( when you look at the directory after linking you should see artist names as folders inside of /music-library )
4. Install node modules ( grunt-js & its dependencies ) : `$ npm install`
5. Run grunt `grunt` or grunt watch `grunt watch`. Running grunt will create updated versions of the /dist css and js files. Grunt Watch will watch the project directory for changes and update /dist as needed.
6. Done! you are ready to pull up the page in a browser

NOTE - If are not on a system that supports symbolic links.. *cough windows* - you can just place the files into the /music-library folder, the expected folder structure inside of /music-library looks like : `/Artist-Name/Album-Name/SongName.mp3` - which happens to be identical to the way itunes will sort your music.

# CONTRIBUTORS

Thanks for feedback, ideas, and suggestions:
David R. Huff