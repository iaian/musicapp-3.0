<?php
/** CLASSES
 *
 */

class MusicLibrary
{
	// location of music
	private $musicLocation = "music-library/";

	public function setMusicLocation($newMusicLocation) {
		$this->musicLocation = $newMusicLocation;
	}

	public function getArtistList()
	{
		// Add cwd to music location
		$dir = $this->musicLocation;

		// use helper function to get file list for the first dir
		$musicDataArray = getDirContents($dir);

		// remove .gitignore from array
		$fileSystemItemsToRemove = '/.git[iI?]gnore/';

		foreach ($musicDataArray as $key => $value) {
			if (preg_match($fileSystemItemsToRemove, $value)) {
				unset($musicDataArray[$key]);
			}
		}

		// sort array
		sort($musicDataArray);

		$musicDataJson = json_encode($musicDataArray);

		// remove null items from JSON
		$musicDataJson = preg_replace('/[nN]{1}[uU]{1}[lL]{2}[,]{1}/', "", $musicDataJson);

		return $musicDataJson;
	}

	public function GetArtistData($artistName)
	{
		// Add cwd to music location
		$this->musicLocation = $this->musicLocation.$artistName.'/';
		$dir = $this->musicLocation;

		// array to hold music data
		$returnDataArray = array();

		// use helper function to get file list for the first dir
		$albumNameArray = getDirContents($dir);

		// sort albumNames
		sort($albumNameArray);

		// get songs for and nest them under this album
		foreach ($albumNameArray as $key => $albumName) {

			$thisDir = $dir.$albumName.'/';
			$thisDirContents = getDirContents($thisDir);

			// sort numarically
			sort($thisDirContents, SORT_NUMERIC);

			array_push($returnDataArray, $albumName, $thisDirContents);
		}
		$musicDataJson = json_encode($returnDataArray);
		return $musicDataJson;
	}

	public function getRandomPlaylist()
	{
		// Add cwd to music location
		$this->musicLocation = $this->musicLocation;
		$dir = $this->musicLocation;

		// array to hold music data
		$allSongsArray = array();

		// use helper function to get file list for the first dir
		$musicDataArray = getDirContents($dir);

		// loop over each artist directory
		foreach ($musicDataArray as $key => $artistName) {

			$thisDir = $dir.$artistName.'/';
			$thisDirContents = getDirContents($thisDir);

			// loop over each album directory
			foreach ($thisDirContents as $albumKey => $albumName) {

				$thisDir2 = $thisDir.$albumName.'/';
				$thisDirContents2 = getDirContents($thisDir2);

				// loop over each song
				foreach ($thisDirContents2 as $key => $song) {
					array_push($allSongsArray, $artistName.'/'.$albumName.'/'.$song);
				}
			}
		}

		// array for holding random playlist
		$randomPlaylistArray = array();
		$totalSongs = count($allSongsArray);

		// detect less than 100 songs in music-library
		if ($totalSongs < 100) {
			$numberOfSongsToPick = $totalSongs;
		} else {
			$numberOfSongsToPick = 100;
		}

		// pick 100 songs at random
		$rand_keys = array_rand($allSongsArray, $numberOfSongsToPick);

		// build randomPlaylist Array
		foreach ($rand_keys as $key => $value) {
			array_push($randomPlaylistArray, $allSongsArray[$value]);
		}

		// DEBUG
		/*
		print_r($allSongsArray);
		echo '<hr/>';
		print_r($rand_keys);
		echo '<hr/>';
		print_r($randomPlaylistArray);
		*/

		// shuffle order
		shuffle($randomPlaylistArray);

		$musicDataJson = json_encode($randomPlaylistArray);

		// remove null items from JSON
		$musicDataJson = preg_replace('/[nN]{1}[uU]{1}[lL]{2}[,]{1}/', "", $musicDataJson);

		return $musicDataJson;
	}

	public function getAllSongs()
	{
		// Add cwd to music location
		$dir = $this->musicLocation;

		// array to hold music data
		$musicDataArray = array();

		// use helper function to get file list for the first dir
		$artistDirArray = getDirContents($dir);

		// sort albumNames
		sort($artistDirArray);

		// create multi dimensional array insert copy of item as fist multi dimensional perimeter
		// get albums
		$artistNameCount = 0;
		foreach ($artistDirArray as $key => $artistName) {

			$thisAlbumDirPath = $dir.$artistName.'/';
			$thisAlbumDir = getDirContents($thisAlbumDirPath);

			// get songs in this album
			$albumNameCount = 0;
			foreach ($thisAlbumDir as $albumKey => $albumName) {

				$thisSongDirPath = $thisAlbumDirPath.$albumName.'/';
				$thisSongDir = getDirContents($thisSongDirPath);

				foreach ($thisSongDir as $key => $value) {
					array_push($musicDataArray, $artistName.'/'.$albumName.'/'.$value);
				}

				$albumNameCount++;
			}
			$artistNameCount++;
		}

		$musicDataJson = json_encode($musicDataArray);

		// remove null items from JSON
		$musicDataJson = preg_replace('/[nN]{1}[uU]{1}[lL]{2}[,]{1}/', "", $musicDataJson);

		return $musicDataJson;
	}
}

class Playlist
{
	private $playlistDirectory = "playlists/";

	public function setPlaylistLocation($newPlaylistLocation) {
		$this->playlistDirectory = $newPlaylistLocation;
	}

	public function savePlaylist($playlistName, $playlistJSON)
	{
		$playlistDirectory = $this->playlistDirectory;

		$fileSystemItemsToRemove = array('../', './', '/');
		$playlistName = str_replace($fileSystemItemsToRemove, "", $playlistName);

		$thisFilesPath = $playlistDirectory.'/'.$playlistName.'.json';

		// test for existance of directory, if not create playlist dir
		if (!is_dir($playlistDirectory)) {
			mkdir($playlistDirectory, 0777);
		}

		// test if filepath is write-able, if not recursively chmod the playlists dir
		if (!is_writable($playlistDirectory)) {
			chmod($playlistDirectory, 0777);
		}

/* Not Needed
		// test for existance of thisFilesPath - if exists set permissions to 777
		if (file_exists($thisFilesPath)) {
			error_log($thisFilesPath);
			chmod($thisFilesPath, 0777);
		}
*/

		// Use these two GET variables to write playlistJSON to file
		$thisFile = fopen($thisFilesPath, 'w');

		$playlistJSON = json_encode($playlistJSON);

		// write json to file
		fwrite($thisFile, $playlistJSON);

		// close file
		fclose($thisFile);

		return 'success';
	}

	public function getPlaylistList()
	{
		$playlistDirectory = $this->playlistDirectory;

		$playlistArray = scandir($playlistDirectory);

		// items to filter out of array
		$fileSystemItemsToRemove = array('/^.$/', '/^..$/', '/.git[iI?]gnore/');
		$playlistArray = preg_replace($fileSystemItemsToRemove, "", $playlistArray);

		// Alphabetize - case insensitive
		natcasesort($playlistArray);

		// remove empty array items and rekey array
		$playlistArray =  array_values(array_filter($playlistArray));

		$playlistArray = json_encode($playlistArray);

		return $playlistArray;
	}

	public function getPlaylist($playlistName)
	{
		$playlistDirectory = $this->playlistDirectory;

		// grab requested json from file system
		$jsonToReturn = json_encode(file_get_contents($playlistDirectory.'/'.$playlistName.'.json'));

		return $jsonToReturn;
	}
}