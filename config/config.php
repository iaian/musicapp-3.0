<?php
/* Setup Default Environment
 *		- setup environment defaults
 *
 */

if (empty($_SESSION)) {
	session_start();
}

// Note these settings can be changed via /AppConfig.php. See sample - /AppConfig.example.php.
$ENABLE_FIREBASE = true;
$FIREBASE_URL = 'YOURFIREBASEURL';
$SITE_NAME = 'Music App 3';
$SITE_PASSWORD = 'YOURSWEETPASSWORD';


/* Check for custom config file
 *		- if found load it
 *		- this file is not in version control, see .gitIgnore
 */

$customConfigPath = $_SERVER["DOCUMENT_ROOT"].'/AppConfig.php';

if (file_exists($customConfigPath)) {
	// Load custom config
	include($customConfigPath);
}


/* Initializers
 *		- set commonly used variables, objects, arrays n such needed in main templates
 *
 */

if(empty($_SESSION['loggedIn'])) {
	$_SESSION['loggedIn'] = 0;
}


/* Included modules
 *		- helpful functions and classes
 */

include('functions.php');
include('musicApp.Classes.php');
