<?php
/* FUNCTIONS */


function getDirContents($dir) {
	$exludedFolderItems = array('..','.','.gitIgnore','.DS_Store', '_','-','null','.gitignore');

	// array to hold return data
	$musicDataArray = array();

	// Open a known directory, and proceed to read its contents
	if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
      while (($file = readdir($dh)) !== false) {
      	// Test array for excludedFolderItems
      	if (
      		in_array($file, $exludedFolderItems)
      		// || preg_match($fileExtensionsToFilter, $file)
      	) {
      		// Excluded items or filtered extensions exist
      	} else {
      		// extension to filter not matched
      		// add all names to array
      		array_push($musicDataArray, $file);
      	}
      }
      closedir($dh);
    }
	}
	return $musicDataArray;
}