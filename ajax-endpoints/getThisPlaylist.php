<?php
include('../config/config.php');

if ($_SESSION['loggedIn'] !== 1) {
  die();
}

if (isset($_GET['name'])) {
  $playlistName = $_GET['name'];
} else {
  die();
}

header('Content-Type: appplication/json; charset=utf-8');

$MusicAppPlaylist = new Playlist;

$setPlaylistLocation = $MusicAppPlaylist->setPlaylistLocation('../playlists/');
$playlist = $MusicAppPlaylist->getPlaylist($playlistName);

echo $playlist;
