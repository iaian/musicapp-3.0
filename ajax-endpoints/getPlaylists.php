<?php
include('../config/config.php');

if ($_SESSION['loggedIn'] !== 1) {
  die();
}

header('Content-Type: appplication/json; charset=utf-8');

$MusicAppPlaylist = new Playlist;

$setPlaylistLocation = $MusicAppPlaylist->setPlaylistLocation('../playlists/');
$playlists = $MusicAppPlaylist->getPlaylistList();

echo $playlists;
