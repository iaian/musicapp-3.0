<?php
include('../config/config.php');

if ($_SESSION['loggedIn'] !== 1) {
  die();
}

$playlistName = $_POST['name'];
$playlistJSON = $_POST['JSON'];

// insure name and playlistJson are not empty
if (empty($playlistName) || empty($playlistJSON)) {
  die();
}

// save json playlist into directory
$MusicAppPlaylist = new Playlist;

$setPlaylistLocation = $MusicAppPlaylist->setPlaylistLocation('../playlists');
$playlistReturn = $MusicAppPlaylist->savePlaylist($playlistName, $playlistJSON);

echo $playlistReturn;
