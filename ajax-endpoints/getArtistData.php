<?php
include('../config/config.php');

if ($_SESSION['loggedIn'] !== 1) {
	die();
}

header('Content-Type: appplication/json; charset=utf-8');

if (isset($_GET['artistName'])) {
	$artistName = $_GET['artistName'];

	$MusicApp = new MusicLibrary;
	$MusicApp->setMusicLocation("../music-library/");
	$thisArtistData = $MusicApp->GetArtistData($artistName);

	echo $thisArtistData;
	die();
}