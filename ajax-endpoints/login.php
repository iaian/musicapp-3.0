<?php
// load config
include('../config/config.php');

/*--- Login ---*/
// insure password isset
if(empty($_POST['pw']) || empty($_POST['name'])) { die(); }

// test for suecessful password
if($_POST['pw'] == $SITE_PASSWORD) {
	// set session flag to log user in
	$_SESSION['loggedIn'] = 1;
	$_SESSION['name'] = $_POST['name'];
	// return status for js
	echo 'success';
} else {
	$_SESSION['loggedIn'] = 0;
	unset($_SESSION['name']);

	echo 'fail';
}
die();
