<?php
// Load config file
include('config/config.php');

if($_SESSION['loggedIn'] !== 1) {
	// User not logged in, show login
	$siteContent = file_get_contents('templates/login.html');
} else {
	// User is logged in, load music app
	$siteContent = file_get_contents('templates/musicApp.html');

	// Get music data JSON for onload
	$MusicApp = new MusicLibrary;

	$artistList = $MusicApp->getArtistList();
	$allSongs = $MusicApp->getAllSongs();

	// Get playlist json
	$MusicAppPlaylist = new Playlist;
	$playlists = $MusicAppPlaylist->getPlaylistList();

	// Use to fill with JS and JSON for use on page load
	$scriptContent = '<script type="text/javascript">'.
							        'var ENABLE_FIREBASE = "'.$ENABLE_FIREBASE.'",
							             FIREBASE_URL = "'.$FIREBASE_URL.'",
                           userName = "'.$_SESSION['name'].'",

                           // Core musicApp object
													 musicAppLibrary = {
															"ArtistNames" : '.$artistList.',
															"AllSongs" : '.$allSongs.',
															"playlists" : '.$playlists.'
													 };
							    	</script>';
}

// Load page template
include_once('templates/main.php');
